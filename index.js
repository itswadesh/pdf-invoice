const PDFDocument = require('pdfkit')
const fs = require('fs')
doc = new PDFDocument()
doc.pipe(fs.createWriteStream('output/output.pdf'))
doc.image('img/logo.png', {
    fit: [250, 300],
    align: 'center',
    valign: 'center'
})

doc.fontSize(15)
    .text('TAX INVOICE', 250, 5)
doc.fontSize(10)
    .text('DELIVERY ADDRESS', 40, 40)
    .text('Kirti')
doc.fontSize(8)
    .text('#349 Sector 18-c near old CentralBank')
    .text('Rourkela')
    .text('CITY:Rourkela,STATE:Odisha')
    .text('PIN:147301')
    .text('MOBILE:805426350')
doc.fontSize(10)
    .text('INVOICE NO# OD001234500', 390, 50)
    .text('COD Collection Amount:Rs.249/-', 390, 90)
doc.rect(380, 40, 180, 30)
    .stroke()
doc.rect(380, 80, 180, 30)
    .stroke()
doc.rect(40, 120, 520, 20)
    .stroke();
doc.rect(40, 120, 520, 55)
    .stroke()
doc.rect(40, 175, 520, 20)
    .stroke()
doc.moveTo(210, 120)
    .lineTo(210, 175)
    .stroke()
doc.moveTo(290, 120)
    .lineTo(290, 175)
    .stroke()
doc.moveTo(430, 120)
    .lineTo(430, 175)
    .stroke()
doc.text('ITEM NAME AND SKU', 50, 125)
doc.text('QTY', 220, 125)
doc.text('VALUE PER QTY', 300, 125)
doc.text('AMOUNT', 450, 125)
doc.fontSize(8)
    .text('Battlane BL-26 Mah Power Bank', 40, 145).text('-Black BL26-Black')
    .text('1', 230, 145).text('249', 310, 145).text('Rs.249', 450, 145).text('Shipping Charges Rs.100|COD Charges Rs.0', 40, 180)
    .text('SHIPPER ADDRESS', 460, 210)
doc.fontSize(8).text('T3 Powerbank', 485, 220).text('SF-705,7th Floor Megapoils,Sec-48,Sohna Road', 365, 230)
    .text('Gurgaon,Haryana-122001', 445, 240)
doc.moveTo(40, 250)
    .lineTo(560, 250)
    .stroke()
doc.fontSize(12)
doc.text('Ordered Via codenx.com', 421, 255).text('codenx.com', 488, 255)
doc.rect(40, 270, 520, 20).stroke()
    .text('RETAIL INVOICE', 250, 275)
    .text('RETAIL INVOICE', 250, 275).text('INVOICE NUMBER:', 330, 300).text('INVOICE DATE:')
doc.fontSize(10).text('RIF-10163-18172', 440, 300)
doc.fontSize(10).text('RIF-10163-18172', 440, 300).text('03-AUG-2018', 420, 315)
doc.rect(40, 330, 520, 20).stroke()
doc.rect(40, 350, 520, 50).stroke()
doc.rect(40, 400, 520, 30).stroke()
doc.moveTo(300, 430)
    .lineTo(300, 330).stroke()
doc.fontSize(12)
doc.text('SELLER', 120, 336).text('BUYER', 400, 336)
    .text('T3 PowerBank', 50, 352)
doc.fontSize(8).text('SF-705,7th Floor,JMD Megapoils,Sec.48,Sohna Road,Rourkela').text('Odisha-122001')
doc.fontSize(12).text('Kirti', 310, 352)
doc.fontSize(8).text('#349 sector3-c near old Central Bank Rourkela')
doc.fontSize(8).text('CITY:Rourkela,STATE:Odisha').text('PIN:769003,MOBILE:9999999999')
doc.fontSize(12).text('DISPATCHED VIA', 50, 410).text('DISPATCH DOC. NO.(AWB)', 310, 410)
doc.fontSize(9).text('Delivery', 150, 410)
    .text('369210029422', 470, 410)
doc.rect(40, 450, 520, 20).stroke()
doc.rect(40, 470, 520, 40).stroke()
doc.rect(40, 510, 520, 20).stroke()
doc.moveTo(90, 450).lineTo(92, 510).stroke()
doc.moveTo(270, 450).lineTo(270, 510).stroke()
doc.moveTo(340, 450).lineTo(340, 510).stroke()
doc.moveTo(450, 450).lineTo(450, 510).stroke()
doc.fontSize(12).text('S.NO', 42, 455)
    .text('ITEM DESCRIPTION', 95, 455)
    .text('QTY', 290, 455).text('RATE', 360, 455).text('AMOUNT', 460, 455)
doc.fontSize(8).text('1', 45, 480)
    .text('Battlane BL-26 2600 Mah PowerBank-Black', 95, 475)
    .text('ORDER NO:10163')
    .text('SUBORDER NO.18172').text('1', 280, 480).text('249', 350, 480).text('249', 470, 480)
doc.fontSize(12).text('TOTAL', 50, 515).text('Rs.249', 470, 515)
doc.fontSize(12).text('AMOUNT IN WORDS:Two Hundred And Forty Nine ONLY', 40, 550)
doc.moveTo(40, 580).lineTo(560, 580).stroke()
doc.moveTo(40, 600).lineTo(560, 600).stroke()
doc.text('THIS IS A COMPUTER GENERATED INVOICE AND DOES NOT REQUIRE SIGNATURE', 40, 585)
